var watch = require('node-watch');
var fs = require('fs');

const dir = '/appdata/tiddlywiki/TiddlyWiki5/Wikis/DungeonImpact/tiddlers'; // directory to watch for changes

const css = 'hidden-references'; // class to hide hardlinks in

var lastFile;

watch(dir, { recursive: false }, function (evt, name) {
    var tiddlerName = name.split('/');
    tiddlerName = tiddlerName[tiddlerName.length - 1];

    // evt can be updat or remove
    // console.dir(evt);
    if (evt == 'update') {
        fs.readFile(name, 'utf8', function (err, tiddler) {
            if (err) {
                return console.error(err);
            }

            if (!tiddler.includes('tmap.id')) {
                // if tiddlymap hasn't updated its ID, return and wait for the incoming update
                console.info('waiting for tiddlymap id');
                return;
            } else if (lastFile == tiddlerName) {
                lastFile = '';
                return;
            }
            lastFile = tiddlerName;

            console.log(`hardlinking ${tiddlerName}`);

            var links = findLinks(tiddler); // get array of links

            // format links into css module
            var module = `\n\n@@.${css}\n`;

            links.forEach(function (link) {
                module += `[[${link}]]`;
            });

            module += '\n@@';

            tiddler = String(tiddler);

            tiddler = removeCSS(tiddler);

            tiddler += module;

            fs.writeFileSync(`../tiddlers/${tiddlerName}`, tiddler, 'utf8');
        });
    } else {
        return;
    }
});

/**
 * removes old hidden css from tiddler
 * @param {string} tiddler
 * @returns {string} tiddler
 */
function removeCSS(tiddler) {
    if (occurrences(tiddler, `@@.${css}`) == 1) {
        tiddler = tiddler.split(`\n\n@@.${css}`);
        return tiddler[0];
    } else {
        return tiddler;
    }
}

/**
 * takes in string of tiddler text, and returns an array of links
 * @param {string} tiddler text of tiddler
 * @returns {array} array of links
 */
function findLinks(tiddler) {
    var links = [];
    tiddler = tiddler.split('<<l');
    for (var i = 1; i < tiddler.length; i++) {
        var fragment = tiddler[i].split('>>');
        fragment = fragment[0]; // we only need everything inside the link macro

        // remove strings that will trip up link detection
        const removeFromFragment = [
            '"sing"',
            "'sing'",
            '"plur"',
            "'plur'",
            '"poss"',
            "'poss'",
            '"pposs"',
            "'pposs'",
        ];
        for (var string in removeFromFragment) {
            fragment = fragment.replace(removeFromFragment[string], '');
        }

        if (fragment.includes('"')) {
            if (occurrences(fragment, '"') == 4) {
                fragment = fragment.split('"')[1];
            } else {
                fragment = /"([^']*)"/.exec(fragment)[1];
            }
        } else if (fragment.includes("'")) {
            if (occurrences(fragment, "'") == 4) {
                fragment = fragment.split("'")[1];
            } else {
                fragment = /'([^']*)'/.exec(fragment)[1];
            }
        }

        fragment = fragment.trim(); // remove whitespace

        links.push(fragment);
    }
    return links;
}

/** Function that count occurrences of a substring in a string;
 * @param {String} string               The string
 * @param {String} subString            The sub string to search for
 * @param {Boolean} [allowOverlapping]  Optional. (Default:false)
 *
 * @author Vitim.us https://gist.github.com/victornpb/7736865
 * @see Unit Test https://jsfiddle.net/Victornpb/5axuh96u/
 * @see http://stackoverflow.com/questions/4009756/how-to-count-string-occurrence-in-string/7924240#7924240
 */
function occurrences(string, subString, allowOverlapping) {
    string += '';
    subString += '';
    if (subString.length <= 0) return string.length + 1;

    var n = 0,
        pos = 0,
        step = allowOverlapping ? 1 : subString.length;

    while (true) {
        pos = string.indexOf(subString, pos);
        if (pos >= 0) {
            ++n;
            pos += step;
        } else break;
    }
    return n;
}
